from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from valorificare.forms import ValorificareForm
from valorificare.models import Valorificare


class ValorificareCreateView(CreateView):
    template_name = 'valorificare/adauga_valorificare.html'
    model = Valorificare
    form_class = ValorificareForm
    success_url = reverse_lazy('home')  # ce scop are acest url

class ValorificareListView(ListView):
    template_name = 'valorificare/lista_valorificare.html'
    model = Valorificare
    context_object_name = "all_valorificare"

    def get_queryset(self):
        return Valorificare.objects.filter(active=True)

class ValorificareUpdateView(UpdateView):
    template_name = 'valorificare/update_valorificare.html'
    model = Valorificare
    form_class = ValorificareForm
    success_url = reverse_lazy('lista_valorificare')




