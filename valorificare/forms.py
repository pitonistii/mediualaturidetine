from django import forms
from django.forms import TextInput

from valorificare.models import Valorificare


class ValorificareForm(forms.ModelForm):
    class Meta:
        model = Valorificare
        fields = '__all__'
        widgets = {
            'firma_valorificare': TextInput(
                attrs={'placeholder': 'Adauga firma valorificare', 'class': 'form-control'}),
            'tip_deseu': TextInput(attrs={'placeholder': 'Adauga tipul de deseu valorificat', 'class': 'form-control'}),
            'cod_deseu': TextInput(attrs={'placeholder': 'Adauga cod deseu valorificat', 'class': 'form-control'}),
            'cantitate_kg': TextInput(attrs={'placeholder': 'Adauga cantitatea valorificata', 'class': 'form-control'}),
            'cost_lei': TextInput(attrs={'placeholder': 'Adauga cost', 'class': 'form-control'}),

        }
