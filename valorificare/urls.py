from django.urls import path
from valorificare import views

urlpatterns = [
    path('adauga-deseu-valorificat', views.ValorificareCreateView.as_view(),
         name='adauga_deseu_valorificare'),
    path('lista-valorificare', views.ValorificareListView.as_view(), name='lista_valorificare'),
    path('update-valorificare/<int:pk>', views.ValorificareUpdateView.as_view(), name='update_valorificare'),
]
