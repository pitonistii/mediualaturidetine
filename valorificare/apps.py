from django.apps import AppConfig


class ValorificareConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'valorificare'
