from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from reciclare.forms import ReciclareForm
from reciclare.models import Reciclare


class ReciclareCreateView(CreateView):
    template_name = 'reciclare/adauga_reciclare.html'
    model = Reciclare
    form_class = ReciclareForm
    success_url = reverse_lazy('home')

class ReciclareListView(ListView):
    template_name = 'reciclare/lista_reciclare.html'
    model = Reciclare
    context_object_name = 'all_reciclare'

    def get_queryset(self):
        return Reciclare.objects.filter(active=True)

class ReciclareUpdateView(UpdateView):
    template_name = 'reciclare/update_reciclare.html'
    model = Reciclare
    form_class = ReciclareForm
    success_url = reverse_lazy('lista_reciclare')


