from django import forms
from django.forms import TextInput

from reciclare.models import Reciclare


class ReciclareForm(forms.ModelForm):
    class Meta:
        model = Reciclare
        fields = '__all__'
        widgets = {
            'firma_reciclatoare': TextInput(
                attrs={'placeholder': 'Adauga firma reciclatoare', 'class': 'form-control'}),
            'tip_deseu': TextInput(attrs={'placeholder': 'Adauga tipul de deseu', 'class': 'form-control'}),
            'cod_deseu': TextInput(attrs={'placeholder': 'Adauga cod deseu', 'class': 'form-control'}),
            'cantitate_kg': TextInput(attrs={'placeholder': 'Adauga cantitatea reciclata', 'class': 'form-control'}),
            'nr_aviz': TextInput(attrs={'placeholder': 'Adauga numar aviz', 'class': 'form-control'}),
            'pret': TextInput(attrs={'placeholder': 'Adauga pret factura', 'class': 'form-control'})

        }

