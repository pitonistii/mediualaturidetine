from django.urls import path
from reciclare import views

urlpatterns = [
    path('adauga-deseu/', views.ReciclareCreateView.as_view(), name='adauga_deseu'),
    path('lista-reciclare', views.ReciclareListView.as_view(), name='lista_reciclare'),
    path('update-reciclare/<int:pk>', views.ReciclareUpdateView.as_view(), name='update_reciclare'),
]
