from django.apps import AppConfig


class ProduseIntroduseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'produse_introduse'
