
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from produse_introduse.forms import ProduseIntroduseForm
from produse_introduse.models import ProduseIntroduse


class ProduseIntroduseCreateView(CreateView):
    template_name = 'produse_introduse/adauga_produse_introduse.html'
    model = ProduseIntroduse
    form_class = ProduseIntroduseForm
    success_url = reverse_lazy('home')

class ProduseIntroduseListView(ListView):
    template_name = 'produse_introduse/list_produse_importate.html'
    model = ProduseIntroduse
    context_object_name = 'all_produse'

    def get_queryset(self):
        return ProduseIntroduse.objects.filter()

class ProduseItroduseUpdateView(UpdateView):
    template_name = ''
    model = ProduseIntroduse
    form_class = ProduseIntroduseForm
    success_url = reverse_lazy('lista_produse_introduse')