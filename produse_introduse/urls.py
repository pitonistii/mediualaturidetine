from django.urls import path

from produse_introduse import views

urlpatterns = [
    path('adauga-produse-introduse', views.ProduseIntroduseCreateView.as_view(),
         name='adauga_produse_introduse'),
    path('lista-produse-introduse', views.ProduseIntroduseListView.as_view(), name='lista_produse_introduse')
]
