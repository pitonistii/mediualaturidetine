from django import forms
from django.forms import TextInput, Select

from produse_introduse.models import ProduseIntroduse


class ProduseIntroduseForm(forms.ModelForm):
    class Meta:
        model = ProduseIntroduse
        fields = '__all__'
        widgets = {
            'luna': Select(attrs={'placeholder': 'Adauga luna ', 'class': 'form-control'}),
            'deseu_introdus': TextInput(attrs={'placeholder': 'Adauga deseu introdus ', 'class': 'form-control'}),
            'cantitate': TextInput(attrs={'placeholder': 'Adauga cantitate ', 'class': 'form-control'}),
            'indeplinire_obiectiv': Select(attrs={'placeholder': 'Indeplinire obiectiv ', 'class': 'form-control'}),
        }
