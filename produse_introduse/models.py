from django.db import models
from django import forms

ALEGERI = [("individual", "individual"), ("OIREP", "OIREP")]
ALEGE_LUNA = [("IANUARIE", "IANUARIE"), ('FEBRUARIE', 'FEBRUARIE'), ('MARTIE', 'MARTIE'),
              ("APRILIE", "APRILIE"), ('MAI', 'MAI'), ('IUNIE', 'IUNIE'), ('IULIE', 'IULIE'), ('AUGUST', 'AUGUST'),
              ('SEPTEMBRIE', 'SEPTEMBRIE'), ('OCTOMBRIE', 'OCTOMBRIE'), ('NOIEMBRIE', 'NOIEMBRIE'),
              ('DECEMBRIE', 'DECEMBRIE')]


class ProduseIntroduse(models.Model):


    alege_luna = models.CharField(choices=ALEGE_LUNA, default='IANUARIE', max_length=10)
    deseu_introdus = models.CharField(max_length=20)
    cantitate = models.IntegerField()
    # indeplinire_obiectiv = models.CharField(max_length=20) #vreau dropdown cu Individual si OIREP
    indeplinire_obiectiv = models.CharField(choices=ALEGERI, default="individual", max_length=10)


    def __str__(self):
        return f'{self.alege_luna} {self.deseu_introdus}'

